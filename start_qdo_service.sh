#!/bin/bash
#
# Starts qdo_werbserver and stores it pid in qdo_service.pid
# Server stoud and stderr is dumped on a file named "qdo_service.(epochnow)"
# 

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
PID_FILE="qdo_service.pid"
echo "Setting up environment"
source virtualenvs/py2.7/bin/activate
echo "Starting qdo REST API Service at: ${DIR}"
EXEC_FILE="app.fcgi"
cd "${DIR}"
DATE=`date +%s`
"${DIR}/${EXEC_FILE}" >  "qdo_service.log.${DATE}" 2>&1 &
echo $! > "${DIR}/$PID_FILE"
echo "Server started"

