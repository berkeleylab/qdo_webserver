import os
import sys

# If using a virtualenv, set this var to a fully qualified
# path to the virutal env bin directory

venv_path = None
# venv_path = '/var/www/venv/bin'

if venv_path:
    activate_this = '{0}/activate_this.py'.format(venv_path.rstrip('/'))
    execfile(activate_this, dict(__file__=activate_this))

# This will set the wsgi to look in the root of the distro 
# deployment for the app.  If you wish to keep the wsgi
# file elsewhere, modify the sys.path.append() bit to 
# include a full path to the deployment.
basedir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(basedir)

def application(req_environ, start_response):
    from qdo_webserver import app as _application
    return _application(req_environ, start_response)