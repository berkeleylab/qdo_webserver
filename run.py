#!/usr/bin/env python
"""
Code to run the development webserver.
"""
from optparse import OptionParser

from qdo_webserver import app

from sremote.tools import get_module_version

import getpass

def main():
    usage = '%prog [ -p port | --ssl  | --ssh  | --debug]'
    parser = OptionParser(usage=usage)
    parser.add_option('-p', '--port', metavar='PORT',
            type='int', dest='port', default=8080,
            help='Port to run server on (default=%default).')
    parser.add_option('-S', '--ssl',
            dest='ssl', action='store_true', default=False,
            help='Run development server under ssl (default=%default).')
    parser.add_option('-s', '--ssh',
            dest='ssh', action='store_true', default=False,
            help=('Run development server under using an ssh remote'+
                   ' connector. If not set it uses NEWT'))
    parser.add_option('-d', '--debug',
            dest='debug', action='store_true', default=False,
            help=('Run development server in debug mode (default=%default).'))
    
    options, args = parser.parse_args()

    _kw = dict(host='0.0.0.0', port=options.port)
    if options.ssl:
        try:
            import OpenSSL
            _kw['ssl_context'] = 'adhoc'
        except ImportError:
            print('The pyOpenSSL lib is required for --ssl (NOT running in SSL mode).')
    else:
        if app.config.get('FORCE_SSL', False):
            print('Not running as --ssl, disabling FORCE_SSL config.py option')
            app.config['FORCE_SSL'] = False
   
    if options.ssh:
        print ("Using a SSH remote connector.")
        app.config["connector"] = 'ssh'
        app.config["ssh_user"] = getpass.getuser()
    else:
        print ("Using a NEWT remote connector.")
        app.config["connector"] = 'newt'
    
    if options.debug:
        print ("Activating Flask debug mode.")
        app.debug = True
        app.config['ENFORCE_SECURE_COOKIES'] = False
    
    app.run(**_kw)

if __name__ == '__main__':
    version_sremote=get_module_version("sremote")
    version_expected=0.14
    if float(get_module_version("sremote"))<version_expected:
        print "ERROR: sremote version ("+version_sremote+") not"+ \
              " good, update to >="+str(version_expected)
    else:
        main()
