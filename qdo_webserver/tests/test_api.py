#!/usr/bin/env python

"""
From top level directory (ie: where run.py is), run the following:

python qdo_webserver/tests/test_api.py 

To run a single test:

python qdo_webserver/tests/test_api.py  TestApi.test_logout



"""


import calendar
import json
import qdo
import os
import shutil
import unittest
import time

from qdo_webserver import app
from qdo import Task

import getpass

API_BASE = '/api/v1'
TRANSIENT_Q = 'transient_queue'
USER = getpass.getuser()

# URIs
# token_endpoint = '{0}/token'.format(API_BASE)
auth_endpoint = '{0}/site/login/'.format(API_BASE)
setup_endpoint = '{0}/site/setup/'.format(API_BASE)
test_endpoint = '{0}/site/testremote/'.format(API_BASE)
q_detail = '{0}/{1}/queues/{2}/'.format(API_BASE, USER, TRANSIENT_Q)
q_list = '{0}/{1}/queues/'.format(API_BASE, USER)
q_retry = '{0}/retry'.format(q_detail)
q_launch = '{0}/{1}/queues/{2}/launch/'.format(API_BASE, USER, TRANSIENT_Q)
q_burndown = '{0}/{1}/queues/{2}/burndown/'.format(API_BASE, USER, TRANSIENT_Q)
task_detail = lambda task_id: '{0}/{1}/queues/{2}/tasks/{3}/'.format(API_BASE, USER, TRANSIENT_Q, task_id)
task_list = '{0}/{1}/queues/{2}/tasks/'.format(API_BASE, USER, TRANSIENT_Q)
task_bulk = '{0}bulk/'.format(task_list)


class TestApi(unittest.TestCase):
    """
Architecture: qdo_webserver is designed to call a remote qdo API using sremote.
In order to do the testing, qdo_webserver is configured to use an ssh connector
against localhost. So The testing will be against the user. This has side
effects to be understood (at least by now):
1-. The tests will install sremote in the user's account.
2.- The tests alter the users queues.
3.- The tests will fail if the users have any pre-existing queue.

TODO(gonzalorodrigo): pass QDO_HOMEDIR to the remote environment so it does
not use the user's sqlite. This would aviod problems with 2 and 3. 
    """
    def setUp(self):

#TODO(gonzalorodrigo): force code not tu use $HOME/.qdo/ for the tests. Right
# now, the QDO_DB_DIR is ignored. It should be passed to sremote which should
# set them it in the remote environment.  

        self._db_dir="/tmp/testdb/"
        os.environ["QDO_DB_DIR"] =  self._db_dir
        os.environ["QDO_BACKEND"] = "sqlite"
        
        self._test_dir="/tmp/qdo_test_launch/"
        if not os.path.exists(self._test_dir):
            os.makedirs(self._test_dir)
        self.addCleanup(self.remove_test_dir)
        
        
        self.addCleanup(self.db_destroy)

        app.testing = True
        
        app.config['REMOTE_ENV'] = {'QDO_DB_DIR':self._db_dir,
                                    'QDO_BACKEND': 'sqlite', 
                                    'QDO_TEST_DIR': self._test_dir,
                                    'QDO_BATCH_PROFILE': 'test-profile'}
        
        current_dir = os.path.dirname(os.path.realpath(__file__));
        app.config['REMOTE_PATH'] = [current_dir]

        # Turn off FORCE_SSL and enable for specific tests
        app.config['FORCE_SSL'] = False
        app.config['connector'] = 'ssh'
        app.config['REMOTE_SITE'] = 'localhost'
        app.debug = True
        print "END STUP"
        
        self.client = app.test_client()
        r = self.client.post(auth_endpoint, data = dict(
                                                    username=getpass.getuser(),
                                                    password =""))
        self.assertEqual(r.status_code, 200)
        
        current_dir = os.path.dirname(os.path.realpath(__file__));
        old_path=os.getenv("PATH", None)
        os.environ["PATH"]=old_path+":"+current_dir


    def db_destroy(self):
            
        try:
            r = self.client.delete(q_detail)
            shutil.rmtree(self._db_dir)
        except:
            pass
    
    def remove_test_dir(self):
        if (self._test_dir):
            shutil.rmtree(self._test_dir)
    
    def test_self_discovery(self):
        #TODO(gonzalordrigo): Implement it
        pass
    
    def test_setup(self):
        app.config['ALLOW_SETUP']=True
        r = self.client.post(setup_endpoint)
        self.assertEqual(r.status_code, 200)
         
    def test_setup_fail(self):
        app.config['ALLOW_SETUP']=False
        r = self.client.post(setup_endpoint)
        self.assertEqual(r.status_code, 401)
    
    def test_api_present(self):
        r = self.client.get('{0}/'.format(API_BASE))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertTrue(data.get('version', False))
        
    
    def test_create(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data.get('qdo_result').get('name', None), TRANSIENT_Q)
    
    def test_list_queues(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(q_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        print "DATA", data['queues']
        self.assertEqual(len(data['queues']), 1)
        self.assertEqual(data['queues'][0]['name'], TRANSIENT_Q)
        self.assertEqual(data['queues'][0]['user'], USER)
        for k,v in data['queues'][0]['ntasks'].items():
            self.assertEqual(v, 0)

    
    def test_add_tasks(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data['task']['task'], t)
        self.assertEqual(data['task']['state'], 'Pending')
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data['task']['task'], t)
        self.assertEqual(data['task']['state'], 'Pending')
        
        r = self.client.get(q_detail)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        data=data.get('qdo_result')
        self.assertEqual(data['ntasks']['Pending'], 2)
        self.assertEqual(data['name'], TRANSIENT_Q)
        self.assertEqual(data['user'], USER)
        self.assertEqual(data['state'], 'Active')
        
    def test_add_multiple_tasks(self):
        q = qdo.create(TRANSIENT_Q)
        previous_id = q.add("echo 0")
        tasks_info =("""{ "tasks": ["echo 1", "echo 2"],
                         "ids": "",
                         "priorities" : ["", "0"],
                         "requires": ["""+
                         '"'+str(previous_id)+'"'+
                         """, ""]
                         }""")

        #tasks_info = json.loads(tasks_info)
        r = self.client.post(task_bulk, data = tasks_info)

        self.assertEqual(r.status_code, 200, r.data)

        new_tasks = dict(json.loads(r.data))["tasks"]
        self.assertTrue(len(new_tasks),2)
        task1 = q.tasks(id=new_tasks[0]["id"])
        task2 = q.tasks(id=new_tasks[1]["id"])
        
        self.assertEqual(task1.task, "echo 1")
        self.assertEqual(task1.state, Task.WAITING)
        
        self.assertEqual(task2.task, "echo 2")
        self.assertEqual(task2.state, Task.PENDING)

    def test_list_tasks(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(task_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 2)
            
    def test_task_details(self):
        # function not implemented yet
        return
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(task_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 2)

        task_id = data['tasks'][0]['id']

        # info on a single task
        r = self.client.get(task_detail(task_id))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data['task']['state'], 'Pending')
        self.assertEqual(data['task']['queuename'], TRANSIENT_Q)
        self.assertEqual(data['task']['id'], task_id)
        
    
    def test_change_task_state(self):
        # function not implemented yet
        
        return
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(task_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 2)

        task_id = data['tasks'][0]['id']
        
            
        r = self.client.put(task_detail(task_id), data=dict(state='Failed'))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data['task']['state'], 'Failed')
        self.assertEqual(data['task']['queuename'], TRANSIENT_Q)
        self.assertEqual(data['task']['id'], task_id)
    
    def test_list_tasks_filtered(self):
        # Uncomment when task manipulation is implemented
        return
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(task_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 2)

        task_id = data['tasks'][0]['id']
        
        # list filtered by state - boo for no get() params=dict arg
        r = self.client.put(task_detail(task_id), data=dict(state='Failed'))
        self.assertEqual(r.status_code, 200)
        r = self.client.get('{0}?state=Failed'.format(task_list))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 1)
        self.assertEqual(data['tasks'][0]['id'], task_id)
        
    def test_pause_activate_queue(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.put(q_detail, data=dict(state='Paused'))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        data=data.get('qdo_result')
        self.assertEqual(data['ntasks']['Pending'], 1)
        self.assertEqual(data['ntasks']['Failed'], 0)
        self.assertEqual(data['name'], TRANSIENT_Q)
        self.assertEqual(data['user'], USER)
        self.assertEqual(data['state'], 'Paused')

        # resume the queue
        r = self.client.put(q_detail, data=dict(state='Active'))
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        data=data.get('qdo_result')
        self.assertEqual(data['ntasks']['Pending'], 1)
        self.assertEqual(data['ntasks']['Failed'], 0)
        self.assertEqual(data['name'], TRANSIENT_Q)
        self.assertEqual(data['user'], USER)
        self.assertEqual(data['state'], 'Active')
        
    def test_retry(self):
        # uncomment when task state set is implemented
        return
        
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
           
        t = 'hello POST 1'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        t = 'hello POST 2'
        r = self.client.post(task_list, data=dict(task=t))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.get(task_list)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(len(data['tasks']), 2)
 
        task_id = data['tasks'][0]['id']
        r = self.client.put(task_detail(task_id), data=dict(state='Failed'))
        self.assertEqual(r.status_code, 200)
        
        r = self.client.put(q_retry)
        self.assertEqual(r.status_code, 200)
        data = json.loads(r.data)
        self.assertEqual(data['ntasks']['Pending'], 2)
        self.assertEqual(data['ntasks']['Failed'], 0)
        self.assertEqual(data['name'], TRANSIENT_Q)
        self.assertEqual(data['user'], USER)
        self.assertEqual(data['state'], 'Active')
        
    def test_exceptions(self):
        r = self.client.post(q_detail)
        self.assertEqual(r.status_code, 200)
        
        r = self.client.put(q_launch, data=dict(nworkers=1, mpack=-1,
                                                pack=True))
        self.assertEqual(r.status_code, 500)
    
        
    def test_burndown(self):
        q=qdo.create(TRANSIENT_Q)
        time_before_create = self.now()-1
        
        task_id=q.add("sleep 3")
        dep_id=q.add("sleep 1; exit 2", requires=task_id)
        time.sleep(2)

                    
        q.do(timeout=0)
        time_after_do = self.now()+2
        
        bd= self.client.get('{0}?epoch_start={1}&epoch_end={2}&bin_size={3}'.
                            format(q_burndown, time_before_create, 
                                   time_after_do, 1))
        bd= json.loads(bd.data)["burndown"]
        
        total=bd['values']['Total']
        running=bd['values'][Task.RUNNING]
        pending=bd['values'][Task.PENDING]
        waiting=bd['values'][Task.WAITING]
        succeded=bd['values'][Task.SUCCEEDED]
        failed=bd['values'][Task.FAILED]
        
        self.assertEqual(total[0], 0)
        
        # After a second 2 tasks, 1 pending, 1 waiting
        
        self.assertEqual(total[1], 2)
        self.assertEqual(pending[1], 1)
        self.assertEqual(waiting[1], 1)        

        self.assertEqual(total[3], 2)
        self.assertEqual(running[3], 1)
        self.assertEqual(waiting[3], 1)
        
        self.assertEqual(total[4], 2)
        self.assertEqual(running[4], 1)
        self.assertEqual(waiting[4], 1)
        
        self.assertEqual(total[-3], 2)
        self.assertEqual(running[-3], 1)
        self.assertEqual(succeded[-3], 1)  
        
        self.assertEqual(total[-1], 2)
        self.assertEqual(succeded[-1], 1)
        self.assertEqual(failed[-1], 1)
        
    def test_launch_path(self):
        q=qdo.create(TRANSIENT_Q)
        
        #- Relative
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out")        
        
        self.assertFalse(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
    
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=8,
                                              njobs=1,
                                              mpiproc_per_worker=0,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=7,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              dest_dir="tmp/launchtest",
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        self.assertTrue(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
        
        os.remove(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out")
        
        #- Relative with ~
        self.assertFalse(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
    
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=8,
                                              njobs=1,
                                              mpiproc_per_worker=0,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=7,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              dest_dir="~/tmp/launchtest",
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        self.assertTrue(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
        
        
        #- Absolute
        self.addCleanup(os.remove, "/tmp/launchtest/qsub_mock_stdout.out")        
        
        self.assertFalse(os.path.exists(
                         "/tmp/launchtest/qsub_mock_stdout.out"))
    
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=8,
                                              njobs=1,
                                              mpiproc_per_worker=0,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=7,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              dest_dir="/tmp/launchtest",
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        self.assertTrue(os.path.exists("/tmp/launchtest/qsub_mock_stdout.out"))
        
        #- Absolute
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/tmp/launchtest2/qsub_mock_stdout.out")     
        
        self.assertFalse(os.path.exists(os.getenv("HOME")+
                         "/tmp/launchtest2/qsub_mock_stdou.out"))
        #- Env variable
        #- MYVAR is set remotely in the setup
    
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=8,
                                              njobs=1,
                                              mpiproc_per_worker=0,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=7,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              dest_dir="$HOME/tmp/launchtest2",
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        self.assertTrue(os.path.exists(os.getenv("HOME")+
                                "/tmp/launchtest2/qsub_mock_stdout.out"))
        
    
    def test_launch_non_mpi(self):
        q=qdo.create(TRANSIENT_Q)

        q.add("sleep 2")
        q.add("sleep 2")
        
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out")        
        
        self.assertFalse(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
    
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=8,
                                              njobs=1,
                                              mpiproc_per_worker=0,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=7,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              dest_dir="tmp/launchtest",
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], TRANSIENT_Q)
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertEqual(len(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"8")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"7")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"qdo do "+TRANSIENT_Q)
        self._read_apprun_files()
        
        self.assertTrue(os.path.exists(os.getenv("HOME")+
                        "/tmp/launchtest/qsub_mock_stdout.out"))
        
    
    def test_launch_mpi_case1(self):
        q=qdo.create(TRANSIENT_Q)
        
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/qsub_mock_stdout.out")
        
        self.assertFalse(os.path.exists(os.getenv("HOME")+
                        "/qsub_mock_stdout.out"))
        q.add("sleep 2")
        q.add("sleep 2")
        
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=2,
                                              njobs=1,
                                              mpiproc_per_worker=3,
                                              cores_per_mpiproc=8, 
                                              cores_per_worker=None,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        qsub_values = self._read_qsub_params()
        print qsub_values
        self.assertEqual(qsub_values["jobname"], TRANSIENT_Q)
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"3")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"8")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()
        
        self.assertTrue(os.path.exists(os.getenv("HOME")+
                        "/qsub_mock_stdout.out"))
        

    def test_launch_mpi_case2(self):
        q=qdo.create(TRANSIENT_Q)
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/qsub_mock_stdout.out")
        q.add("sleep 2")
        q.add("sleep 2")
        
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=2,
                                              njobs=1,
                                              mpiproc_per_worker=3,
                                              cores_per_mpiproc=None, 
                                              cores_per_worker=48,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], TRANSIENT_Q)
        self.assertEqual(qsub_values["ncores"], "96")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"3")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"12")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()
        
    def test_launch_mpi_case3(self):
        q=qdo.create(TRANSIENT_Q)
        self.addCleanup(os.remove, os.getenv("HOME")+
                        "/qsub_mock_stdout.out")
        q.add("sleep 2")
        
        r = self.client.put(q_launch, data=dict(site="localhost",
                                              nworkers=1,
                                              njobs=1,
                                              mpiproc_per_worker=4,
                                              cores_per_mpiproc=4, 
                                              cores_per_worker=48,
                                              numa_pack=True, script=None,
                                              timeout=0,
                                              runtime=None,
                                              batchqueue="myqueue",
                                              walltime="00:02:00",
                                              batch_opts='-A account',
                                              verbose=True))
        
        self.assertEqual(r.status_code, 200)
        
        qsub_values = self._read_qsub_params()
        self.assertEqual(qsub_values["jobname"], TRANSIENT_Q)
        self.assertEqual(qsub_values["ncores"], "48")
        self.assertEqual(qsub_values["walltime"], "00:02:00")
        self.assertEqual(qsub_values["batchqueue"], "myqueue")
        self.assertEqual(qsub_values["batchopts"], "-A account")
        
        time_stamps, apprun_params=self._read_apprun_files()
        #- Sequential execution
        self.assertLessEqual(self._get_max_dif(time_stamps), 1)

        self.assertEqual(apprun_params["nPEs"],"4")
        self.assertEqual(apprun_params["PEs_per_node"],"2")
        self.assertEqual(apprun_params["cores_per_PE"],"4")
        self.assertEqual(apprun_params["PEs_per_numa_node"],"1")
        self.assertEqual(apprun_params["task"],"sleep 2")
        self._read_apprun_files()
    
    def test_logout(self):
        response = self.client.delete(auth_endpoint)
        self.assertEqual(response.status_code, 200)
        r = self.client.get(q_list)
        self.assertEqual(r.status_code, 401)
       
    
    def now(self):
        return calendar.timegm(time.gmtime())
        return int(time.time())
    
    def _read_qsub_params(self):
        qsub_file=open(self._test_dir+"qsub_mock.out", "r") 
        lines = qsub_file.readlines()
        values = dict()
        for line in lines:
            parts = line.split("|")
            key=parts[0]
            if (len(parts)>1):
                value ="|".join(parts[1:])
            else:
                value=None
            if key in ["jobname", "batchqueue"]:
                values[key]=value.strip()
            elif key == "ncoreswall":
                sub_values_list=value.strip().split(" ")
                for sub_value in sub_values_list:
                    sub_value=sub_value.split("=")
                    key=sub_value[0]
                    if (key=="mppwidth"):
                        values["ncores"]=sub_value[-1]
                    elif (key=="walltime"):
                        values["walltime"]=sub_value[-1]
                    
            elif key == "rest_params":
                sub_values=value.strip().split()
                values["script"]=sub_values[-1]
                values["batchopts"]=" ".join(sub_values[:-1])
        qsub_file.close()
        return values

    def _read_apprun_files(self):
        file_names = os.listdir(self._test_dir)
        time_stamps= []
        single_file=None
        for name in sorted(file_names):
            if not single_file:
                single_file=name
            if "aprun_mock" in name:
                words = name.split(".")
                stamp=words[2]
                if not stamp in time_stamps:
                    time_stamps.append(stamp) 
        return time_stamps, self._read_apprun_file_params(self._test_dir+
                                                          single_file)
        
    
    def _read_apprun_file_params(self, file_route):
        aprun_file=open(file_route, "r") 
        lines = aprun_file.readlines()
        values=dict()
        for line in lines:
            line=line.strip()
            words=line.split("|")
            key=words[0]
            value=None
            if len(words):
                value="|".join(words[1:])
            if (key =="qdo_call"):
                values["task"]=value
            else:
                values[key] = value
            
        aprun_file.close()
        return values
    
    def _get_max_dif(self, stamps):
        """Returns maximum difference in timestamps. Used to avoid flackyness
        in time tests."""
        if len(stamps)==1:
            return 0
        return int(stamps[-1])-int(stamps[0])
    
if __name__ == '__main__':
    unittest.main()