"""
Views for the REST API component.
"""

import config
from flask import Blueprint, current_app
import flask
from flask_cors import cross_origin
import qdo
from qdo import Task, Queue
from qdo_webserver.remote import ClientAPI
import qdo_webserver.lib.json_messages as jm
from qdo_webserver.lib.helpers import check_auth, auth_ok_action,  \
    auth_cookies_required, ssl_required, \
    credential_header, remotecheck, handle_sremote_exceptions, nocache, \
    create_client, logout_action


    
# Register as a blueprint.
api = Blueprint('api', __name__, url_prefix='/api/v1')

# Routes for this blueprint.
@api.route('/')
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
def start():
    r = dict()
    r['version'] = qdo.__version__
    r['info'] = """\
QDO (kew-doo) is a lightweight toolkit for processing many tasks in a queue.
This is the web api for that toolkit.  Start at {0}/username for
a list of queues for that user.""".format(api.url_prefix)
    return flask.jsonify(r)

@api.route('/site/login/',  methods=['GET', 'POST', 'DELETE'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
def auth_process():
    """ Authorization process.
    POST: Connects to the execution host an does the authorization process
        with username and password. Username and authorization token are
        stored in the client cookies.
     
        Args as POST variables:
            username: string with the execution host user username.
            password: stting with the execution host user passwword.
        
        Returns: 
            If successful, it returns JSON dictionary with the username and
            the authorization token. It also stores the username, token,
            home_dir, sremote_dir and sremote_tmp in the client cookies. It
            returns 401 if authorization failed.
        
    GET: Checks if the client cookies username and authorization token is valid.
        Args in cookies:
            qdo_username: string with username to check for.
            qdo_authkey: authorization key associated with a session. Created
              and stored by the auth_process POST. 

        Returns: 
            JSON dictionary with the username and the authorization token is
            authorization is successful. 401 if authorization failed or cookies
            are missing.
    """
    client, do_selfdiscover = create_client()
    
    if flask.request.method == 'POST':
        username = flask.request.form.get('username')
        password = flask.request.form.get('password')
        
        result = client.auth(username, password, 
                             do_selfdiscover=do_selfdiscover)
        print "RESULT", result
        if not result:
            return jm.error_401("Auth failed against site")
        else:
            # Following methods sets the cookies.
            home_dir = client.get_home_dir()
            if do_selfdiscover:
                client.do_self_discovery()
            # we store whatever dirs the client uses.
            sremote_dir = client.get_dir_sremote()
            sremote_tmp = client.get_dir_tmp()
            return auth_ok_action(username, result, home_dir, sremote_dir,
                                  sremote_tmp)
    elif flask.request.method == "GET":
        result, username = check_auth(client, do_selfdiscover)
        if (result==None):
            return jm.error_401("Auth cookies missing")
        if (not result):
            return jm.error_401("Auth failed against site")
        else:
            print username, result
            return auth_ok_action(username, result)
    elif flask.request.method == "DELETE":
        return logout_action(client)
        
@api.route('/site/setup/', methods=['POST'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
def remote_setup(client):
    """ Authorization process.
    POST: Deploys SREMOTE and QDO in the the execution host site. 
        
        Returns: 
            200 if it successes. 401 otherwise.
    """
    if not current_app.config.get("ALLOW_SETUP"):
        return jm.error_401("Setup not authorized") 
    if (client.do_bootstrap_install()):
        client.do_install_git_module(
             "https://gonzalorodrigo@bitbucket.org/berkeleylab/qdo.git", 
             "master", "qdo")
        return jm.payload_200(dict(resp="Ok"))
    return jm.error_401("Error installing SREMOTE/QDO")

@api.route('/<username>/',  methods=['GET'])
@api.route('/<username>/queues/',  methods=['GET'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def qlist(client, username):
    """ List a user's queues summary info. 
    GET:  For each queue (at least): execution status, number of tasks of each
    type, and REST url to access its detailed information.
        Args in url:
            username: string of user's username which queues will be listed.
        Returns: 
            A queue_list_payload with the queues summary info.
    """
    queues = client.qlist(user=username)
#     if not type(queues) is list :
#         return jm.error_400("User not found")
    resp=jm.queue_list_payload(username, queues)
    #resp.headers['Access-Control-Allow-Credentials'] = 'true'
    return resp

@api.route('/<username>/queues/<queuename>/',  methods=['GET', 'POST', 'PUT', 
                                                        'DELETE'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def queue(client, username, queuename):
    """Queue summary info. 
        Args in url:
            username: string of queue owner's username.
            queuename: string of with the name of the queue.
        Args in PUT:
            state: can take values in qod.Queue.VALID_STATES.
        Returns: 
            A queues summary info: execution status, number of tasks of each
            type, and a REST url to access its task list.
        Verbs:
            - GET: execution status, number of tasks of each type, and a REST
              url to access its task list.
            - POST: creates a new queue.
            - PUT: changes state of the queue.
            - DELETE: destroys queue.
    """
    if flask.request.method == 'GET':
        status = client.status(queuename, user=username)
        if status == None:
            return jm.error_404("Queue not found.")
    elif flask.request.method == 'POST':
        status = client.create(queuename, user=username)
        if status == None:
            return jm.error_400("Queue already existing or creation failed")
    elif flask.request.method == "PUT":
        # Check input for valid information
        if not flask.request.values.has_key('state'):
            return jm.error_400("PUT request did not contain 'state' param.")
        elif flask.request.values['state'] not in Queue.VALID_STATES:
            valid_args = Queue.VALID_STATES
            s = "Bad state value '{0}' - permitted args: {1}".format(
                                flask.request.values['state'], valid_args)
            return jm.error_400(s)
        else:
            status = client.set_state(queuename, 
                                      flask.request.values['state'],
                                      user=username)
            if status == None:
                return jm.error_400("Queue does not exit or error setting "
                                    "state")
    elif flask.request.method == "DELETE":
        result=client.delete_queue(queuename, user=username)
        if result==None:
            return jm.error_404("Queue not found.")
        elif not result:
            return jm.error_404("Error deleting queue.")
        else:
            return jm.payload_200(dict(ok="Queue "+queuename+" deleted."))

    print status
    return jm.queue_status_payload(status, flask.request, username, queuename )

@api.route('/<username>/queues/<queuename>/tasks/',  methods=['GET', 'POST'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def tasks(client, username, queuename):
    """Queue Tasks manipulation. 
        Args in url:
            username: string of queue owner's username.
            queuename: string of with the name of the queue.
        Args in POST:
            task: can take values in qod.Queue.VALID_STATES.
            'other': other arguments for task creation.
        Returns: 
            GET: List of tasks in the queue. 
            POST: Task created data.
        Verbs:
            - GET: List tasks of the queue.
            - POST: Add a task to the queue. Post variable task is used as the
              task. Other variables will be passed to the task creation method.
    """
    if flask.request.method == 'GET':
        state=None
        if flask.request.values.has_key('state'):
            state = flask.request.values['state']
            if  not state in Task.VALID_STATES:
                return jm.error_400('{0} is not a valid task state.'.format(
                                    state))
                
        tasks = client.get_tasks(queuename,
                             state =state,
                             user=username)
        if tasks == None:
            return jm.error_404("Queue not found.") 
        return jm.multiple_task_payload(tasks, flask.request, username,
                                        queuename)
 
    elif flask.request.method == 'POST':
        if not flask.request.values.has_key('task'):
            return jm.error_400('POST request requires a task parameter')
        else:
            args = flask.request.values.to_dict()
            task = args.pop('task')
            new_task = client.add_task(queuename, task, user=username, 
                                       **args)
            print "task", new_task
            if new_task == None:
                return jm.error_400("Error in task parameters")
            
            return jm.single_task_payload(new_task, flask.request, username,
                                          queuename)
            
@api.route('/<username>/queues/<queuename>/tasks/bulk/',  methods=['POST'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def bulk(client, username, queuename):
    """Queue Tasks manipulation. 
        Args in url:
            username: string of queue owner's username.
            queuename: string of with the name of the queue.
        Args in POST:
            Tasks are sent as posted json: A dictionary with four keys (
            'tasks','ids', 'priorities', 'requires')
            - tasks: A list of strings with the actions of each task.
            - ids: a list of strings  with the id of each task. If a task id is
                not to be set, then the position takes the value ""
            - priorities: a list of ints with the prioirity of each task. If
                a task prioirity is not to be set, then the postition takes
                the value "".
            - requires: a list of ids, or list of ids, for the tasks that have
                to be completed before a task can start. If a task has no
                dependencies, its posittion will be set to "".
            
        About the Post Args: 
            - Same positions in the list represent the same task.
            - ids, priorities, requires may be set to "" to expres that they
              do not contain information for any task. However if set, they
              must contain the same number of items as tasks.
            - ids, priorities, requires, may contain empty elements (e.g.
              a task does not depend on other). This is represented by an
              empty string ("")
            
        Returns: 
            POST: Tasks created data.
        Verbs:
            - POST: Add tasks to the queue.
    """
    if flask.request.method == 'POST':
        try:
            payload = dict(flask.request.get_json(force=True))
        except:
            return jm.error_400('Did not receive valid json as POST data')
        required_keys = [u'tasks', u'requires', u'ids', u'priorities']
    
        if set(payload.keys()) != set(required_keys):
            return jm.error_400("Incomplete json: expected payload with four "
                "keys: {0} - got: {1}".format('/'.join(required_keys), 
                '/'.join(payload.keys())))
        
        for key_name in [u"ids", u"requires", u"priorities"]:
            payload[key_name]=_clean_list(payload[key_name])
        new_tasks = client.add_multiple_tasks(qname=queuename,
                            tasks=payload.get('tasks'), user=username, 
                            ids=payload.get('ids'),
                            priorities=payload.get('priorities'),
                            requires=payload.get('requires'))
        if new_tasks == None:
            return jm.error_400("Error in task parameters")
        
        return jm.multiple_task_payload(new_tasks, flask.request, username,
                                      queuename)

def _clean_list(value_list):
    if value_list is None:
        return None
    if value_list is str and value_list.strip() == "":
        return None
    if not value_list:
        return None
    return [str(x) if (x is not None and x!="") else None for x in value_list]


@api.route('/<username>/queues/<queuename>/retry/', methods=['PUT'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def retry_queue(client, username, queuename):
    """Set jobs in queue to be retried if necessary (PUT)."""
    if flask.request.method == 'PUT':
        status=client.retry_queue(queuename, user=username)
        if status == None:
            return jm.queue_404(username, queuename)
        return jm.queue_status_payload(status, flask.request, username, queuename )

@api.route('/<username>/queues/<queuename>/recover/', methods=['PUT'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def recover_queue(client, username, queuename):
    """Reset running tasks to pending."""
    if flask.request.method == 'PUT':
        status=client.recover_queue(queuename, user=username)
        if status == None:
            return jm.queue_404(username, queuename)
        return jm.queue_status_payload(status, flask.request, username,
                                       queuename)

@api.route('/<username>/queues/<queuename>/rerun/', methods=['PUT'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def rerun_queue(client, username, queuename):
    """Reset completed tasks to pending."""
    if flask.request.method == 'PUT':
        status=client.rerun_queue(queuename, user=username)
        if status == None:
            return jm.queue_404(username, queuename)
        return jm.queue_status_payload(status, flask.request, username, 
                                       queuename)

@api.route('/<username>/queues/<queuename>/launch/', methods=['PUT'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def launch_queue(client, username, queuename):
    """Launches wokers on the remote site. PUT parameters equivalent to the ones
    in backend.launch.
    
    URL Args:
        username:
        queuename
        
    PUT variables Args:
        site: required string, site to run the job at. Use simple hostname e.g.
            edison, hopper.
        nworkers: optional int, number of workers to launch. Default 1.
        njobs: optional int, number of jobs on which to map the workers.
            Default 1
        mpiproc_per_worker: optional int, number of MPI process per worker.
            Default 0
        cores_per_mpiproc: Semi-optional int. If mpiproc_per_worker > 0, 
            cores_per_mpiproc and/or cores_per_worker have to be set.            
        cores_per_worker: Semi-optional int, f mpiproc_per_worker > 0, 
            cores_per_mpiproc and/or cores_per_worker have to be set. if
            mpiproc_per_worker==0 it must be set.
        script: optional string, if set qdo will append script in front of the
            executed tasks.
        numa_pack: optional boolean (True/False). If True, workers and MPI procs
            will not allocate cores across Numas. Default False.
        timeout: optional float. If set, workers will wait timeout seconds
            for new tasks before ending execution.
        runtime: optional int. if set, the workers won't run more than runtime
            seconds (it does not interrupt task execution).
        batchqueue: required string, scheduling queue to run the job on.  
        walltime: optional string in format hh:mm:ss. If set, its value will be
            used as walltime run limit for the jobs.
        batch_opts: optional string. If set batch_opts will be passed to the
            job submission command.
        dest_dir: optional string. Sets the the route where the output of the
            batch jobs will be written to. It admits three types of parameters:
            - Absolute route: starts with '/'.
            - Relative to the user HOME: otherwise.
            Env variables values are admitted in the route. eg:
                $LAUNCH_DIR/queue1/
        verbose: optional bool, produces extra internal info on the launch.
    IMPORTANT NOTE:
        If the user does not specify a value, do not pass timeout, runtime, 
        walltime, batch_opts. script to the call. 
    Returns 
        200 if job submit succeded.
        400 If errro in input parameters.
    """
    if flask.request.method == 'PUT':
  
        qname = queuename
        user = username
        args = flask.request.values.to_dict()
        
        nworkers = 1
        
        remote_site=None
        
        njobs = 1
        mpiproc_per_worker=0
        cores_per_mpiproc=None
        cores_per_worker=None
        numa_pack=False
        script=None
        timeout=None
        runtime=None
        batchqueue=None
        walltime=None
        batch_opts=''
        verbose=False
        dest_dir=None
                   
        try:     
            if args.has_key('site'):
                remote_site = args.pop('site')   
            if args.has_key('nworkers'):
                nworkers = int(args.pop('nworkers'))
            if args.has_key('njobs'):
                njobs = int(args.pop('njobs'))
            if args.has_key('cores_per_mpiproc'):
                cores_per_mpiproc = int(args.pop('cores_per_mpiproc'))
            if args.has_key('mpiproc_per_worker'):
                mpiproc_per_worker = int(args.pop('mpiproc_per_worker'))
            if args.has_key('cores_per_worker'):
                cores_per_worker = int(args.pop('cores_per_worker'))
            if args.has_key('script'):
                script = args.pop('script')
            if args.has_key('numa_pack'):
                numa_pack = args.pop('numa_pack').lower() == "true"
            if args.has_key('timeout'):
                timeout = int(args.pop('timeout'))
            if args.has_key('runtime'):
                runtime = float(args.pop('runtime'))
            if args.has_key('batchqueue'):
                batchqueue = args.pop('batchqueue')
            if args.has_key('walltime'):
                walltime = args.pop('walltime')
            if args.has_key('batch_opts'):
                batch_opts = args.pop('batch_opts')
            if args.has_key('verbose'):
                verbose = args.pop('verbose').lower() == "true"
            dest_dir_rel=None
            dest_dir_abs=None
            if args.has_key('dest_dir'):
                dest_dir=args.pop('dest_dir')
                if dest_dir:
                    if dest_dir[0]=="/" or dest_dir[0]=="$" :
                        dest_dir_abs=dest_dir
                    elif dest_dir:
                        dest_dir_rel = dest_dir
                        if dest_dir_rel[0]=="~":
                            dest_dir_rel=dest_dir_rel[2:]
                
                
        except ValueError as  e:
            return jm.error_400(str(e))
                
        launch_client, self_disc = create_client(remote_site=remote_site,
                                                 pwd_dir=dest_dir_abs,
                                                 pwd_at_home_dir=dest_dir_rel)
        
        if not launch_client.check_auth(username, client.get_token()):
            return jm.error_400("Auth against "+remote_site+" for user "+
                                username +"with existing credentials "+
                                "failed")

            
        packages_dir = launch_client.get_packges_dir()
        launch_client.register_remote_env_variable('QDO_DIR', 
                                                   packages_dir+"/qdo",
                                                   only_if_no_set=True)    
        
        status=launch_client.launch_queue(
                   qname=qname, nworkers=nworkers, user=user, njobs = njobs,
                   mpiproc_per_worker=mpiproc_per_worker, 
                   cores_per_mpiproc=cores_per_mpiproc, 
                   cores_per_worker=cores_per_worker,
                   numa_pack=numa_pack,
                   script=script, timeout=timeout, runtime=runtime,
                   batchqueue=batchqueue, walltime=walltime,
                   batch_opts=batch_opts, verbose=verbose,
                   export_all_env_vars=False,
                   extra_vars_to_export=["SREMOTE_BACKUP_ENV"])
        if status == None:
            return jm.queue_404(username, queuename)
        return jm.queue_status_payload(status, flask.request, username,
                                       queuename)
        
@api.route('/<username>/queues/<queuename>/burndown/', methods=['GET'])
@ssl_required
@credential_header
@cross_origin(origins=config.QDO_WEBSITE_DOMAIN)
@nocache
@handle_sremote_exceptions
@auth_cookies_required
@remotecheck
def burndown(client, username, queuename):
    """Produces the burndown data as a JSON dict for the queue queue_name
    Args in GET:
        epoch_start: long with epoch time stamp where to start the chart data.
        epoch_ned: long with epoch time stamp where to end the chart data.
        bin_size: long in seconds of the size of the time to group data
    Returns:
        "burnddown" in the response data with json dic equivalent to the qdo
        burndown call.
     """
    if flask.request.method == 'GET':
        values=flask.request.values
        if (not values.has_key('epoch_start') or 
            not values.has_key('epoch_end') or not values.has_key('bin_size')):
            return jm.error_400("Missing input parameters. Required "
                                "epoch_start, epoch_end, bin_size")

        burndown_summary = client.burndown_queue(qname=queuename, user=username,
                                epoch_start=values['epoch_start'],
                                epoch_end=values['epoch_end'],
                                bin_size=values['bin_size'])
        if burndown_summary == None:
            return jm.queue_404(username, queuename)
        
        resp = jm.create_base_payload()
        resp["burndown"] = burndown_summary
        return flask.jsonify(resp)
    