import sremote.api
import sremote.tools
import sremote.connector.newt as newt
import sremote.connector.ssh as ssh
import qdo_webserver 



class ClientAPI(sremote.api.RemoteClient):
    """
    Class to interact with the remote execution host and do the QDO calls. It
    takes the responsibility to create the corresponding connector and pass it
    to the constructor of the parent class RemoteClient.
    
    SREMOTE is used to execute code remotely and there are different ways to
    discover to use the remote system:
    - Location file: It tries to download a file from $HOME/.sremote that
      specifies the location of the entry point and temporay space.
    - User default config: will use $HOME/.sremote as an entry point and
      temporary space.
    - Fixed folders: will use (sremote_dir) as the sremote entry point and
      $HOME/(home_tmp_dir) as the temporary space.
      
    If sremote_dir and home_tmp_dir are set, that fixed folders method will
    be used. If not, it will first try to use the location file. If the file
    is not present of bad formated it will use the used default config.
    
    Supported constructors:
    - SSH: in debug mode, if qdo_serever.app.debug 
    - NEWT: if not in debug mode.
    """

    def __init__(self, site="edison", sremote_dir=None, home_tmp_dir=None,
                 location_file=None, pwd_at_home_dir=None, pwd_dir=None):
        """Init of the class. It creates the connector towards the remote
        execution host site.
        Args:
            site: string identifying the remote site where to execute code.
            sremote_dir: if set, it configures absolute location of the
                sremote execution files.
            home_tmp_dir: if set, it configures the subfolder of the user's
                home directory where temporary files will be created.
            location_file: name of the file to search in $HOME/.sremote in
                self discovery process.
        """
        #TODO(gonzalorodrigo): use client by other configuration parameters
        # from the app, instead of debug.
        #TODO(gonzalorodrigo): cleaner way of choosing the site and the 
        # connector type.
        if location_file:
            self.sremote_location_file = location_file
            
        if (qdo_webserver.app.config["connector"] == 'ssh'):
            site="localhost"
            conn=ssh.ClientSSHConnector(site)
            conn._token="TOKEN"
            if pwd_at_home_dir:
                conn.set_pwd_at_home_dir(pwd_at_home_dir)
            if pwd_dir:
                conn.set_pwd_dir(pwd_dir)
            super(ClientAPI,self).__init__(conn)
        elif (qdo_webserver.app.config["connector"] == 'newt'):
            newt_connector=newt.ClientNEWTConnector(site)
            super(ClientAPI,self).__init__(newt_connector)
            if sremote_dir:
                newt_connector.set_sremote_dir(sremote_dir)
            if home_tmp_dir:
                newt_connector.set_tmp_at_home_dir(home_tmp_dir)
            if pwd_at_home_dir:
                newt_connector.set_pwd_at_home_dir(pwd_at_home_dir)
            if pwd_dir:
                newt_connector.set_pwd_dir(pwd_dir)
        else:
            raise RuntimeError("Connector type unknown: "
                               +qdo_webserver.app.config["connector"] )
        self.register_remote_module("qdo")
        
    def auth(self, username, password, do_selfdiscover=False):
        """Performs authorization against site with username and password.
        if do_self_disovered it configures the sremote endpoint by doint the
        sremote discovery process.""" 
        if (self._comms_client.auth(username,password)):
            if do_selfdiscover:
                self._comms_client.do_self_discovery(self.sremote_location_file)
            return self._comms_client._token
        else:
            return False
    
    def do_self_discovery(self):
        return self._comms_client.do_self_discovery(self.sremote_location_file)
        
    
    def check_auth(self, username, token, home_dir=None, sremote_dir=None,
                   sremote_tmp=None):
        """Cheks if username and token are valid for remote execution host.
        Configures client reference dirs or discover them.
        Args:
            username: auth username.
            token: auth token.
            home_dir: if set, configures the user home directory used by the
                client.
            sremote_dir, sremote_tmp: if both set, it configures the absolute
                file routes used to locate the sremote endpoint.
            do_selfdiscover: if true and sremote_tmp or sremote_dir not set
                it configures the Client to do the discovery process.
        
        Returns: Auth token if auth successful, False otherwise.
        """ 
        self._comms_client.auth(username, token=token, home_dir=home_dir)
        if (sremote_dir and sremote_tmp):
            self._comms_client.set_sremote_dir(sremote_dir)
            self._comms_client.set_tmp_dir(sremote_tmp)
        return self._comms_client._token
    
    def get_token(self):
        return self._comms_client._token
    
    def test_installed(self):
        """Checks if the remote host can execute sremote calls."""
        try:
            return_value, out  = self.do_remote_call("qdo.remote", 
                                                     "get_version")
            print return_value, out
            return return_value
        except sremote.tools.ExceptionRemoteNotSetup as e:
            print "SREMOTE lib test failed:", str(e)
            return False
        
    # QDO specific calls. They use sremote to invoke remote functions.
    def qlist(self, user=None):
        return_value, out  = self.do_remote_call("qdo.remote", "qlist", 
                                              args={'user':user})
        return return_value
    
    def status(self, qname, user=None):
        return_value, out  = self.do_remote_call("qdo.remote", "status", 
                                      args={'qname':qname,'user':user})
        print out
        return return_value
    
    def create(self, qname, user=None):
        return_value, out  = self.do_remote_call("qdo.remote", "create", 
                                      args={'qname':qname,'user':user})
        return return_value
    
    def set_state(self, qname, state, user=None):
        return_value, out  = self.do_remote_call("qdo.remote", "set_state", 
                                      args={'qname':qname, 'state':state,
                                            'user':user})
        return return_value
    
    
    def get_tasks(self, qname, state=None, user=None):
        return_value, out  = self.do_remote_call("qdo.remote", "get_tasks", 
                                      args={'qname':qname, 'state':state,
                                            'user':user})
        return return_value
    
    def add_task(self, qname, task, user=None, **kwargs):
        args={'qname':qname, 'task':task,'user':user}
        args.update(kwargs)
        return_value, out  = self.do_remote_call("qdo.remote", "add_task", 
                                      args=args)
        return return_value
    
    def retry_queue(self, qname, user):
        args={'qname':qname, 'user':user}
        return_value, out  = self.do_remote_call("qdo.remote", "retry_queue", 
                                      args=args)
        return return_value
    
    def recover_queue(self, qname, user):
        args={'qname':qname, 'user':user}
        return_value, out  = self.do_remote_call("qdo.remote", "recover_queue", 
                                      args=args)
        return return_value

    def rerun_queue(self, qname, user):
        args={'qname':qname, 'user':user}
        return_value, out  = self.do_remote_call("qdo.remote", "rerun_queue", 
                                      args=args)
        return return_value
    
    def delete_queue(self, qname, user):
        args={'qname':qname, 'user':user}
        return_value, out  = self.do_remote_call("qdo.remote", "delete_queue", 
                                      args=args)
        return return_value
    
    def launch_queue(self, qname, nworkers, user=None, njobs = 1,
                   mpiproc_per_worker=0, cores_per_mpiproc=None, 
                   cores_per_worker=1,
                   numa_pack=False,
                   script=None, timeout=None, runtime=None, batchqueue=None,
                   walltime=None,
                   batch_opts='', verbose=False,
                   export_all_env_vars=False, extra_vars_to_export=[]):
        
        args = {'qname':qname, 'user':user, 'nworkers':nworkers,
                'njobs':njobs,'mpiproc_per_worker':mpiproc_per_worker, 
                'cores_per_mpiproc':cores_per_mpiproc, 
                'cores_per_worker':cores_per_worker,
                'numa_pack':numa_pack, 'script': script, 
                'timeout':timeout, 'runtime':runtime, 'batchqueue':batchqueue,
                'walltime':walltime, 'batch_opts':batch_opts, 
                'verbose':verbose,
                'export_all_env_vars':export_all_env_vars,
                'extra_vars_to_export':extra_vars_to_export }
        
        return_value, out  = self.do_remote_call("qdo.remote", "launch_queue", 
                                      args=args, keep_env=True)
        return return_value  
    
    def burndown_queue(self, qname, user, epoch_start, epoch_end, bin_size):
        args={'qname':qname, 'user':user, 'epoch_start':epoch_start, 
              'epoch_end': epoch_end, 'bin_size':bin_size}
        return_value, out  = self.do_remote_call("qdo.remote", "burndown_queue", 
                                      args=args)
        return return_value
    
    def add_multiple_tasks(self, qname, tasks, user=None, ids=None, priorities=None,
                           requires=None):
        
        args=dict(qname=qname, tasks=tasks, user=user, ids=ids,
                  priorities=priorities, requires=requires)
        return_value, out = self.do_remote_call("qdo.remote",
                                                "add_multiple_tasks", 
                                                args=args)
        return return_value
    
    # Helper calls to access parameters from the _comms_client.    
    def get_home_dir(self):
        return self._comms_client._home_dir
    
    def get_packges_dir(self):
        return self._comms_client.get_dir_sremote()+"/tmp"
    
    def get_dir_sremote(self):
        return self._comms_client.get_dir_sremote()
        
    def get_dir_tmp(self):
        return self._comms_client.get_dir_tmp()
    
    def set_dir_sremote(self, sremote_dir):
        self._comms_client.set_sremote_dir(sremote_dir)
    
    def set_dir_tmp(self, tmp_dir):
        self._comms_client.set_tmp_dir(tmp_dir)
        
    def set_dir_tmp_at_home(self, tmp_dir):
        self._comms_client.set_tmp_dir(tmp_dir)
    
        
    
    