"""
Functions/utilities for producing the json messages response to the views.py
module.
"""

import datetime
import flask
from flask import url_for


# -- Error responses

def _timestamp():
    """Returns current timestampe in UTC"""
    return str(datetime.datetime.utcnow())

def error_308(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 308)

def error_309(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 309)

def error_304(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 304)

def error_400(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 400)

def error_404(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 404)

def error_401(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 401)

def error_402(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 402)

def error_403(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 403)

def error_500(msg):
    ret = dict(error = msg)
    return flask.make_response(flask.jsonify(ret), 500)


# -- REST call payloads

def payload_200(obj):
    print "OBJ", obj
    return flask.make_response(flask.jsonify(obj), 200)

def create_base_payload(qdo_result=None, extra_items=None):
    """Creates base payload including: timestamp, resource url and parent
    resource url and signature (if needed")"""
    result = dict(
        time = _timestamp(),
        links = dict(
            resource_url = flask.request.base_url,
            parent_url = flask.request.base_url 
        ),
    )
    if extra_items!=None:
        result.update(extra_items)
        
    if qdo_result!=None:
        result["qdo_result"]=qdo_result
    return result

def queue_list_payload(username, summary_queues):
    """
    Produces a payload with a componente queues that includes a list
    of summaries of queues.
    
    Args:
        username: string with username.
        summary_queues: list of dictionaries. Each dictionary must have at 
            least a component name with a string.
    
    Returns: a json content to be returned.        
    """
    result = create_base_payload()
    result["queues"] = list()
    for q in summary_queues:
        x = dict(
            q,
            detail_url = url_for('api.queue', username=username, 
                                 queuename=q['name'], _external=True)
        )
        result['queues'].append(x)
    return flask.jsonify(result)
    

# TODO(gonzalorodrigo): From this point code still under evaluation.
def queue_404(username, queuename):
    ret = dict(error = """Queue '{0}' not found.  Start at {1}{2}/ for
     a list of queues for that user.""".format(queuename, url_for('api.start'), 
     username))
    return flask.make_response(flask.jsonify(ret), 404)

def task_404(taskid):
    ret = dict(error = "Taskid '{0}' not found.".format(taskid))
    return flask.make_response(flask.jsonify(ret), 404)

def pop_404(taskid):
    ret = dict(error = "No tasks to pop.".format(taskid))
    return flask.make_response(flask.jsonify(ret), 404)


def queue_status_payload(status, req, username, queuename):
    resp = create_base_payload(status)
    new_links=dict(detail_url = url_for('api.tasks',
                                           username=username,
                                           queuename=queuename,
                                           _external=True),
                parent_url = url_for('api.qlist', username=username, 
                                   _external=True))
    resp["links"].update(new_links)
    return flask.jsonify(resp)

def single_task_payload(tsk, req, username, queuename, add_detail=True):
    """Format the json payload for a single task detail.  May or may not
    add a detail_url - will not if the user has drilled down to the ultimate 
    detail, will upon modification, etc"""
    result = dict(
        task = tsk,
        signature = None,
        time = _timestamp(),
        links = dict(
            resource_url = req.base_url,
            parent_url = url_for('api.tasks', username=username, 
                                 queuename=queuename, _external=True)
        )
    )
    return flask.jsonify(result)

def multiple_task_payload(tasks, req, username, queuename):
    """Format the json for a list of tasks in a queue"""
    try:
        parent_url = url_for('api.queue', username=username, 
                             queuename=queuename, _external=True)
    except Exception as e:
        print "Ex", e
    print username, queuename
    result = dict(
        tasks = tasks,
        signature = None,
        time = _timestamp(),
        links = dict(
            resource_url = req.base_url,
            parent_url = parent_url
        )
    )
    return flask.jsonify(result)
