"""
Code to handle authentication. It includes the decorators that wrap the REST
API calls from views.py.

The authentication parameters (username, password) are provided by the user.
Those parameters are used against the NEWT authorization system (doing a REST
call). If validated, NEWT returns a temporary authorization token. That token,
username, and other working values are then stored as cookies in the user's
browser. 

These other values are the user's home folder, and sremote configuration. They
are values that the REST server needs in each call but that won't change
within the same sessions, so reading them once it is enough. Using this
stored values reduces the latency of the calls in 2-3s.
"""


import datetime
import flask
from flask import make_response, request, redirect, current_app
from functools import wraps
import getpass
import qdo_webserver.lib.json_messages as jm
from qdo_webserver.lib.json_messages import create_base_payload
from qdo_webserver.remote import ClientAPI
import sremote.tools
from sremote.tools import ExceptionRemoteNotSetup, ExceptionRemoteModulesError

def check_auth(client):
    """
    Retrieves the cookies qdo_authkey and qdo_username from the user client
    and checks if they are valid to operate against the remote host client.
    
    Args:
        client: string, hostname of the remote host to check against.
        do_selfdiscover: do sremote selfdiscover process if the cookies 
            sremote_dir and sremote_tmp are not set.
    Args in cookies:
        qdo_username: string with username to check for.
        qdo_authkey: authorization key associated with a session. Created
            and stored by the auth_process POST. 
        sremote_dir: string with an absolute remote filesystem route pointing
            to the sremote endpoint.
        sremote_tmp: string with an absolute remote filesystem route pointing
            to the temporary sremote user folder.
    
    Returns:
        None if the cookies are not set. False if the check fails.
        The authorization token if the check is successful. Also the username
        read from the cookie.
    """
    qdo_authkey = flask.request.cookies.get('qdo_authkey')
    username = flask.request.cookies.get('qdo_username')
    home_dir = flask.request.cookies.get('qdo_home_dir', None)
    
    sremote_dir = flask.request.cookies.get('sremote_dir', None)
    sremote_tmp = flask.request.cookies.get('sremote_tmp', None)
        
    if (not username or not qdo_authkey):
        return None, None
    if current_app.testing:
        return client.check_auth(getpass.getuser(), token="mytoken"), getpass.getuser()
    return client.check_auth(username, token=qdo_authkey,
                             home_dir=home_dir,
                             sremote_dir=sremote_dir,
                             sremote_tmp=sremote_tmp), \
                              username

def nocache(view):
    """
    This decorator forces the client not to catch the output of the wrapped
    REST call.
    """
    @wraps(view)
    def no_cache(*args, **kwargs):
        response = make_response(view(*args, **kwargs))
        response.headers['Last-Modified'] = datetime.datetime.now()
        response.headers['Cache-Control'] = ('no-store, no-cache, '+ 
                    'must-revalidate, post-check=0, pre-check=0, max-age=0')
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response
        
    return no_cache

def handle_sremote_exceptions(fn):
    """
    This decorator handles remote exceptions when execution methods through
    sremote. If a remote exception happens, it produces 500 message and
    corresponding error message.  
    """
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except sremote.tools.ExceptionRemoteExecError as e:
            return jm.error_500("Error on remote execution: "+str(e))
    return decorated_function



def auth_cookies_required(fn):
    """
    Use this decorator on views to explicitly require a authorization. It will
    retrieve the username and auth token from the client cookies and check it
    against the execution site.  It also creates and configures a ClientAPI
    that is passed to the decorated function. This function should use 
    this object to interact with the remote host.

    This MUST be used on a view method that has a site parsed 
    from the route. It will call the "decorated" function
    
    Args in cookies:
        qdo_username: string with username to check for.
        qdo_authkey: authorization key associated with a session. Created
            and stored by the auth_process POST.
        
    Returns:
        If auth successful: Decorated function, with an argument "client"
            populated with a ClientAPI object that is configured to connect
            to site with qdo_username and qdo_authkey.
        if cookies are missing: 401
        if authorization failed: 402
    """
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        client, do_selfdiscover = create_client()
        result, username = check_auth(client)
        if (result==None):
            return jm.error_401("Auth cookies missing. Is SSL present? "
                                "Is the referred site authorized to use this "
                                "API?")
        if (not result):
            return jm.error_402("Auth failed against site")
        else:
            kwargs["client"] = client
            return fn(*args, **kwargs)
    return decorated_function

# Custom decorators
def remotecheck(fn):
    """
    This decorator is responsible of managing differences between the local
    and remote libraries versions for sremote (code 308) and qdo (code 309).
    """
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        try:
            resp = fn(*args, **kwargs)
        except ExceptionRemoteNotSetup as e:
            return jm.error_308("SREMOTE LIB on remote host not present: "+
                                str(e)) 
        except ExceptionRemoteModulesError as e:
            return jm.error_309("Required modules version remote-local "+
                                "miss-match: "+
                                str(e)) 
        return resp
    return decorated_function

def credential_header(fn):
    """
    Use this decorator on views to explicitly set the response header
    'Access-Control-Allow-CredentialsAccess-Control-Allow-Credential' to 
    'True'.
    """
    @wraps(fn)
    def decorated_function(*args, **kwargs):
        
        resp = fn(*args, **kwargs)
        resp.headers['Access-Control-Allow-Credentials'] = 'true'
        return resp
    return decorated_function



def ssl_required(fn):
    """
    Decorator to force some vies to use https if FORCE_SSL = True in 
    config.py.

    Credit: http://flask.pocoo.org/snippets/93/
    """
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_app.config.get("FORCE_SSL"):
            if request.is_secure:
                return fn(*args, **kwargs)
            else:
                return redirect(request.url.replace("http://", "https://"))
        
        return fn(*args, **kwargs)
            
    return decorated_view

# Auxiliary methods required by the decorators.

def auth_ok_action(username, tok, home_dir=None, sremote_dir=None,
                   sremote_tmp=None):
    """
    Returns the response to the auth POST and GET requests. It
    also sets the client cookies qdo_auth_keys, qdo_username,
    sremote_dir and sremote_tmp. All the cookies are set as secure cookies 
    (only can be sent through SSL).
    
    Args:
        username: string with the authorized username.
        tok: string with the newt token received after authorization.
        home_dir: (optional) string with an remote filesystem absolute route
            pointing to the user's home directory.
        sremote_dir: (optional) string with an remote filesystem absolute route
            pointing to sremote entry points.
        sremote_tmp: (optional) string with an remote filesystem absolute route
            pointing to a folder where sermote will store its temporary files
            and application results. The user must have write permissions in
            such folder.
    """
    result = dict(
        qdo_authkey = tok,
        username = username,
    )

    payload = create_base_payload(extra_items=result)
    response = make_response(flask.jsonify(payload))
    
    secure_cookies =  current_app.config.get("ENFORCE_SECURE_COOKIES")
    response.set_cookie('qdo_authkey',tok, secure=secure_cookies)
    response.set_cookie('qdo_username', username, secure=secure_cookies)
    if home_dir:
        response.set_cookie('qdo_home_dir', home_dir, secure=secure_cookies)
    if sremote_dir:
        response.set_cookie('sremote_dir', sremote_dir, secure=secure_cookies)
    if sremote_tmp:
        response.set_cookie('sremote_tmp', sremote_tmp, secure=secure_cookies)
    return response

def logout_action(client):
    payload = create_base_payload()
    response = make_response(flask.jsonify(payload))
    secure_cookies =  current_app.config.get("ENFORCE_SECURE_COOKIES")
    response.set_cookie('qdo_authkey','', secure=secure_cookies, expires=0)
    response.set_cookie('qdo_username', '', secure=secure_cookies, 
                        expires=0)
    response.set_cookie('qdo_home_dir', '', secure=secure_cookies,
                        expires=0)
    response.set_cookie('sremote_dir', '', secure=secure_cookies,
                        expires=0)
    response.set_cookie('sremote_tmp', '', secure=secure_cookies,
                        expires=0)
    return response

def create_client(remote_site=None, pwd_at_home_dir=None, pwd_dir=None):
    """
    Creates a ClientAPI and configures it according to the config.py. If
    the sremote_dir or home_tmp_dir are not set, it means that the client
    should use the default dirs and try to run the sremote self discover
    process.
    
    Returns:
        client: Configured ClienAPI
        do_selfdiscove: indicates that the client should do the self discover
            process in the future.
    """
    sremote_dir=get_sremote_dir()
    home_tmp_dir=get_sremote_home_tmp()
    location_file=get_sremote_location_file()
    do_selfdiscover = location_file!=None and location_file!=""
    if remote_site is None:
        remote_site=current_app.config['REMOTE_SITE']
    client = ClientAPI(site=remote_site,
                       sremote_dir=sremote_dir, 
                       home_tmp_dir=home_tmp_dir,
                       location_file=location_file,
                       pwd_at_home_dir=pwd_at_home_dir,
                       pwd_dir=pwd_dir)
    if current_app.config['REMOTE_ENV']:
        for (var_name, var_value) in current_app.config['REMOTE_ENV'
                                                        ].iteritems():
            client.register_remote_env_variable(var_name, var_value)
    if current_app.config['REMOTE_PATH']:
        for path in current_app.config['REMOTE_PATH']:
            client.register_remote_env_path(path)
    
    return client, do_selfdiscover

def get_sremote_location_file():
    if "SREMOTE_LOCATION_FILE" in current_app.config.keys():
        return current_app.config["SREMOTE_LOCATION_FILE"]
    return None
def get_sremote_dir():
    if "REMOTE_SREMOTE_DIR" in current_app.config.keys():
        return current_app.config["REMOTE_SREMOTE_DIR"]
    return None
def get_sremote_home_tmp():
    if "SREMOTE_HOME_TMP" in current_app.config.keys():
        return current_app.config["SREMOTE_HOME_TMP"]
    return None
