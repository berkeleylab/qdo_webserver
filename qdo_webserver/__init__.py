"""
Code for the main flask application.  It pulls in the various components
as flask blueprints.

http://flask.pocoo.org/docs/blueprints/
"""

import logging
import os

from flask import Flask, g

from qdo_webserver.api.views import api

# Set up app and initialize the DB

app = Flask(__name__)
app.config.from_object('config')

# Register blueprints

app.register_blueprint(api)

# Processing callbacks

@app.before_first_request
def setup():
    if app.config.get('ENABLE_DEBUG_LOG', None):
        handler = logging.FileHandler('./{0}'.format(app.config.get('ENABLE_DEBUG_LOG')))
        handler.setLevel(logging.DEBUG)
        formatter = logging.Formatter(app.config.get('DEBUG_LOG_FORMAT'))
        handler.setFormatter(formatter)
        app.logger.addHandler(handler)

# Import the views so everything can use the blueprint 
# @route decorators defined in the views.
import qdo_webserver.api.views
