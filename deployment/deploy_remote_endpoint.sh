#!/bin/bash
#
# This script installs a remote endpoint to be used with qdo at the place where
# it is invoked e.g:
# 
# ~/$ cd folder
# ~/folder/$ ./deploy_remote_endpoint.sh
#
# It will install all required in 'folder'. After configuraion 'folder' can be
# used as the REMOTE_SREMOTE_DIR for qdo_webserver.



INSTALL_DIR=`pwd`
TMP_DIR="${INSTALL_DIR}/tmp"
mkdir "${TMP_DIR}"

echo "Installing and compiling python 2.7.9 + virtualenv 13.1.2"

mkdir ${INSTALL_DIR}/src
mkdir ${INSTALL_DIR}/.localpython
cd ${INSTALL_DIR}/src
wget http://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
tar -zxvf Python-2.7.9.tgz
cd Python-2.7.9


make clean
./configure --prefix=${INSTALL_DIR}/.localpython
make
make install

#install virtualenv

cd ${INSTALL_DIR}/src

wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-13.1.2.tar.gz#md5=b989598f068d64b32dead530eb25589a --no-check-certificate
tar -zxvf virtualenv-13.1.2.tar.gz
cd virtualenv-13.1.2/
${INSTALL_DIR}/.localpython/bin/python setup.py install

cd ${INSTALL_DIR}
mkdir virtualenvs
cd virtualenvs
${INSTALL_DIR}/.localpython/bin/virtualenv py2.7 --python=${INSTALL_DIR}/.localpython/bin/python2.7

echo "Activating installed virtualenv" 
source ${INSTALL_DIR}/virtualenvs/py2.7/bin/activate

echo "Installing libraries QDO+sremote"
# First we download what we need.i
cd "${TMP_DIR}"
git clone https://bitbucket.org/berkeleylab/qdo.git qdo

git clone https://github.com/gonzalorodrigo/sremote.git sremote


# installing libraries in the virutal environmeent

cd "${TMP_DIR}/qdo/py"
python setup.py install
cd "${TMP_DIR}/sremote/py"
python setup.py install

cd "${INSTALL_DIR}"
echo "Copying files"
cp  "${TMP_DIR}/sremote/py/sremote/res/interpreter.sh" .
cp  "${TMP_DIR}/sremote/py/sremote/res/remote_server.py" .

echo "Setting folder permissions"
chmod o=g -R .
chmod -R o-w .
