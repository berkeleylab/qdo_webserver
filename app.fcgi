#!/usr/bin/env python
import os
from flup.server.fcgi import WSGIServer
from qdo_webserver import app
from sremote.tools import get_module_version

BIND_ADDRESS = ('127.0.0.1', 7676)

def main():
    print 'Binding to', BIND_ADDRESS
    app.config["connector"] = 'newt'
    WSGIServer(app, bindAddress=BIND_ADDRESS).run()

if __name__ == '__main__':
    version_sremote=get_module_version("sremote")
    version_expected=0.14
    if float(get_module_version("sremote"))<version_expected:
        print "ERROR: sremote version ("+version_sremote+") not"+ \
              " good, update to >="+str(version_expected)
    else:
    	main()

