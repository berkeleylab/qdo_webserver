qdo_webserver
=============

This is a Flask web framework to interact with the QDO workflow queuing
system.  It exposes a REST api for programmatic interaction and later
a web ui front end.

How this works
==============

The qdo webserver offers and API to run remote qdo commands in the context of a
user within a NERSC system. It uses the sremote library to do python remote 
calls over NEWT that will invoke qdo in a NERSC system.

                       Science Gateway          Newt Server             Edison
                  ========================      ==========    ==============================
    REST Call --> |REST SERVER | sremote | ---> | Newt @ | -> | sremote |-> q=qdo.connect()|
    (list queues) |            |  client |(rest)| NERSC  |    | server  |   q.list()       |
                  ========================      ==========    ==============================
                                                                            |
    JSON response <---------------------------------------------------------|
    
The systems requires depends on two libraries:

* QDO: Qdo basic functions. Required in the remote endpoint. Can be found at:
https://bitbucket.org/berkeleylab/qdo

* SREMOTE: Simplre remote library to execute python code in a remote machine.
It requies NEWT or SSH access on the remote machine. Can be found at:
https://github.com/gonzalorodrigo/sremote 

                  
Installation
============

Install the base sremote package into a virtualenv or your python installation 
of choice. 


This package will be deployed "in place" rather than installed into a 
python site-packages directory via the standard setup.py/etc method.  Deploy
the package where you want it to live, make sure the python installation
(virtualenv, etc) where you have installed sremote is active and execute the
following command from the root of the distro to install the package
requirements:

    pip install -r requirements.txt

The file config.py at the root of the distro contains configuration 
options.  The file as-is presents sane defaults.  



To verify that this initial configuration was successful, execute the 
following command from the root of the distro to run the local 
development server:

    ./run.py --debug

(if debug is not run, the cookies don't work, unless SSL is present.).

You will see output like "* Running on http://0.0.0.0:8080/" - this is where
the dev webserver is running.  Now execute the following command with curl
with the appropriate hostname base and username/password pair valid on the
remote site.

    curl -k -c newt_cookies.txt -X POST -d 'username=user&password=pass' http://127.0.0.1:8080/api/v1/site/login/

And you should see output like this::

    {
    
        "links": {
        "parent_url": "http://127.0.0.1:8080/api/v1/site/login/", 
        "resource_url": "http://127.0.0.1:8080/api/v1/site/login/"
    }, 
        "qdo_authkey": "12345678901234667789001235790921830932", 
        "time": "2015-08-11 02:30:07.773044", 
        "username": "gprodri"
    }

If so, the auth is possible. 

Executing test suite
====================

The basic installation instructions deploy the server itself, makes sure that 
the user database is properly set up and API authentication is working properly. 
To ensure that the api is now properly interacting with the base QDO install and configuration (queue creation and interaction), execute the test suite from the 
root of the distro:

    python qdo_webserver/tests/test_api.py
    
The tests will exercise all of the endpoints in the API and the corresponding 
QDO commands.  The local development server (./run.py) is NOT needed 
as the unit tests use the build-in Flask test_client.  The tests will also use 
a separate in-memory sqlite database to test the authentication, so they will 
not impact your exisiting user db or require that a test user be added 
manually.

IMPORTANT: Tests will interact with an srmote as it is installed in your local
user account. It uses the ssh connector instead of the newt connecter. 

If any errors are generated, verify that the QDO install can properly 
create/etc queues.

Initial concepts
==============
- Webservice: This code to execute remote calls of QDO on an execution host.
- Execution host: the machine were the qdo system is living and on which
backend they are executed.
- sremote: remotting library that connects to the execution host and executes
the qdo calls.
- connector: System used by sremote to execute commands in the OS of the
execution host (e.g. newt, ssh).

Authentication
==============

Overview
--------

This webservice acts as a forwarderd for the authentication operations, it
relays on the authentication method used by the connector accessing the
execution host. This design assures that a weaker auth schema does not weaken
the security of the execution host.

A client uses the webservice auth end point:
- It provides the connector specific auth credentials (e.g. user password).
- The webservice checks the credentials against the execution host connector.
- If credentials are valid, the connector provides an auth token.
- The username and auth token are stored in the cookies of the client.
- Each time that the client calls a method that requires authentication, the
cookies are checked and used as the authentication info.


Authentication end point
------------------------
URL: /api/v1/site/login

POST
Arguments:
    username: string, credential username.
    password: string, credential password.
    
Reponse:
- Auth Valid:
  Code 200
  Content: Json with the following format:
    {
      "links": {
        "parent_url": "http://{baseurl}/site/login/", 
        "resource_url": "http://{baseurl}/site/login/"
      }, 
      "qdo_authkey": "generated token to be used ", 
      "signature": null, 
      "time": "2015-06-04 00:01:22.117121", 
      "username": "username provided in call"
    }
  Cookies: This method sets two cookies in the client: qdo_username and
  qdo_authkey to be used by the authorization functions. 
  From this point the client contains the required authentication info to do
  other operations.
- Auth invalid:      
  Code 401
  Content: Error message.
  
DELETE
It logg.out the user. Auth cookies are removed.
  
Endpoints requiring authentication
----------------------------------
The only requirement for a client to use an endpoint that requires
authentication is to contain valid cookies set by the authentication end
point. 


Remoting setup
==============
The webservice relies on the sremote python library to execute qdo calls on 
the execution host. This requires that the account of the user is correctly
configured. If sremote is missing of its version is inccorect, the API calls
will return 308. Also, the remote QDO has to be the same version as the local
one, in case of missmatch, the API calls will return 309.

The REST serve can be configured to use the remote code in three different ways:

- 1) By reading a file in the user's $HOME/.sremote/.qdo (file name configurable
  in config.py with SREMOTE_LOCATION_FILE).
  
- 2) If the file is not found, using directly ./sremote

- 3) Specifying in the config.py remote file system routes where the
  sremote/qdo code is present and the results will be written:
  REMOTE_SREMOTE_DIR and SREMOTE_HOME_TMP.

Location file
-------------
By default, the location file is called ".qdo" and writen in JSON format. 
Different applications use different location files poiting to directories
where their corresponding libraries are installed.

An example location file content with a tmp relative route is:

    {
        "sremote" : "/global/u1/g/gprodri/shared_qdo",
        "relative_tmp" : "qdo_self"
    }

It will use the /global/u1/g/gprodri/shared_qdo to locate the sremote scripts,
and $HOME/qdo_self as the directory to write temporary files and the launch
results.

Another example of location file content with an absolute temporary route is:

    {
        "sremote" : "/global/u1/g/gprodri/shared_qdo",
        "absolute_tmp" : "/global/u1/g/gprodri/output_qdo"
    }

It will use the /global/u1/g/gprodri/shared_qdo to locate the sremote scripts,
and /global/u1/g/gprodri/output_qdo as the directory to write temporary files and the launch
results. 


Remote Setup
------------
No matter what folders are configured, the REST API can try to install the
sremote and qdo modules in the remote system. This is done by calling the setup
endpoint. However:

- The user has to have write permissions on the configured folders (no matter
  the method.
- Setup has to be enabled in config.py: ALLOW_SETUP=True. If it is set to False
  the call will return 401.
  
URL: /api/v1/site/setup

POST

Requires valid authentication
    
Reponse:
- If installation works:
  Code 200
  Content: Json with the following format:
  
    {
      "links": {
        "parent_url": "http://{baseurl}/site/login/", 
        "resource_url": "http://{baseurl}/site/login/"
      }, 
      "result": "true", 
      "signature": null, 
      "time": "2015-06-04 00:01:22.117121", 
    }
    
- If it fails      
  Code 401
  Content: Error message.


Version Check and Error Codes
=============================
QDO Rest API is a component in a chain of multiple interconnected layers.
As new features come they impact all layers and there is a need for a
mechanism to check that layers are synchronized in the same versions. 

All API calls except login, setup, and test_remote do the following checks:

- srermote remote execution environment is present in the remote user
  environment.
  
- sremote library is present in the remote user environment.

- qdo library is present in the remote user enviroment.

- Version check of module used by REST API and the user remote enviroment is
  the same for: sremote and QDO.

  
All the API calls except login do the following check:

- Client has authorization 


All the calls do the following check:

- Remote call ends correctly without raising exceptions.
   
These are the return codes generated if the
checks fail:

- Code 500, Execution error: call of remote Python method raised an exception.
  The message of the exception and extra debugging information is included in
  the response.
  
- Code 401, Auth info missing. The client browser does not have the auth
  cookies.
  
- Code 402, auth failed. The user and token read from the cookies were used
  to authorize the user but the authorization failed.
  
- Code 308: remote sremote enviroment not present, or version incorrect. It
  means that the sremote version used by the rest API and the one installed
  in the user are different. Requires reinstallation of correct version on
  remote user (setup call). The concrete problem is included in the content
  of the response.
  
- Code 309: Incorrect version of remote modules (or missing) at the remote
  user environment. If the version QDO code installed in the remote does not
  match the the one used by the REST API this code will appaer. It requires
  reinstallation of the remote environment (setup call).
  
  
Other return codes:

- Code 404: resource is missing. e.g. returner if we try to retrieve info
  from a queue, user, or task that does not exist.
  
- Code 400: the input paremeters lead to an operation that cannot be completed.
  e.g.: create a queue that already exists, set a non existing state to a 
  queue.
  


Launching JOBS
==============

The REST API allows to start workers at the compute sites to process the 
tasks in qdo. It uses the following endpoint:

URL: /api/v1/<user>/queues/<queue>/launch/

VERB: PUT

POST Variables:

a) Related to job submission

- site: optional (Default configured site) string with the name of the host
      to connect to (edison, hopper)
      
- nworkers: optional int (Default 1), number of workers to be launched.

- njobs: number of jobs among to spawn the workers.

- walltime: optional string (Default 00:30:00). Wall clock time limit for
  workers.
  
- batch_opts: optional string. Batch options to add to the submission
  command.
  
b) Worker/Task defitinion

- numa\_pack: optional bool (True/False, Default False). If true *workers/mpi
  PEs* won't allocate cores accross numas.
  
- mpiproc\_per\_worker: optional int (default 0) MPI PEs per worker.

- cores\_per\_mpiproc: int, number of cores by MPI PE. At least one of
  cores\_per\_mpiproc or cores\_per\_worker must be set.

- cores\_per\_worker: int, number of cores per worker. At least one of
  cores\_per\_mpiproc or cores\_per\_worker must be set.

c) Related to qdo task execution
 
- script: optional string with the submission script to be used. If not set
  it will use the default scripts.
 
- timeout: optional int (Default 60). Time to wait before giving up.

- runtime: optional float (Default, not set). Quite after running jobs for
  these many seconds.
  
- batchqueue: optional string (Default regular). Submission queue on which
  submit the workers.
 
  
- verbose: optional bool (True/False, default False). Activate verbose 
  mode.
      
      
Using *site*: The configured 'site' must share the same auth domain (same valid
cookies) than the default site. Also, it must have the qdo and sremote
installation.

Special return code 400: Since the launch can be done against other sites,
it is not sure that the auth info is also valid. If the auth info is not valid
for that site the 400 code will be generated.


REST API
========

Quick Reference
---------------
Implemented:

    /api/v1/site/login - get an auth token
    /api/v1/site/setup
    /api/v1/<user>/queues/ - list queues
    /api/v1/<user>/queues/<queue> - view/create/modify named queue
    /api/v1/<user>/queues/<queue>/tasks/ - list/create tasks in queue
    /api/v1/<user>/queues/<queue>/retry/ - retry failed taskes in queue
    /api/v1/<user>/queues/<queue>/recover/ - set running tasks as pending
    /api/v1/<user>/queues/<queue>/rerun/ -  set copmleted tasks as pending
    /api/v1/<user>/queues/<queue>/launch/ -  launch a remote worker to process queue
    /api/v1/<user>/queues/<queue>/retry/ - retry failed taskes in queue
    /api/v1/<user>/queues/<queue>/burndown/?epoch_start=val1&epoch_end&=val2&bin_size=size
         - return a json dictionary containing the data for a burndown chart
         of the tasks tates in the queue  

Not implemented:
  
    
    /api/v1/<user>/queues/<queue>/tasks/pop - get task/change to RUNNING
    /api/v1/<user>/queues/<queue>/tasks/<taskid> - view/modify a task


Full set of api commands
------------------------
TODO(gonzalorodrigo): review this.

This series of commands (adapted from the unit test) shows the flow 
of the endpoints, how to use GET/POST/PUT/DELETE and what params are
sent with various calls:

    # Authorize client, so auth cookies are stored in clien.
    self.client = app.test_client()
    self.client.post(/api/v1/site/login, data = dict(username="username",
                                                    password ="password"))

    # create test queue 'transient_queue'
    r = self.client.post("/api/v1/unittest/queues/transient_queue")

    # check current queues
    r = self.client.get("/api/v1/unittest/queues/")
    data = json.loads(r.data)

    self.assertEqual(len(data['queues']), 1)
    self.assertEqual(data['queues'][0]['name'], TRANSIENT_Q)
    self.assertEqual(data['queues'][0]['user'], USER)

    # Add tasks
    t = "hello POST 1"
    r = self.client.post("/api/v1/unittest/queues/transient_queue/tasks/", 
                          data=dict(task=t))

    t = "hello POST 2"
    r = self.client.post("/api/v1/unittest/queues/transient_queue/tasks/",
                          data=dict(task=t))

    # queue status detail - check tasks
    r = self.client.get("/api/v1/unittest/queues/transient_queue")

    # list o' tasks and snag a task id
    r = self.client.get("/api/v1/unittest/queues/transient_queue/tasks/")
    data = json.loads(r.data)

    task_id = data["tasks"][0]["id"]

    # list filtered by state
    r = self.client.get("/api/v1/unittest/queues/transient_queue/tasks/?state=Failed",
        headers=self.token_auth_header)

    # pause the queue
    r = self.client.put("/api/v1/unittest/queues/transient_queue", 
        data=dict(state="Paused"), headers=self.token_auth_header)

    # resume the queue
    r = self.client.put("/api/v1/unittest/queues/transient_queue", 
        data=dict(state="Active"), headers=self.token_auth_header)

    # retry jobs that need it.
    r = self.client.put("/api/v1/unittest/queues/transient_queue/retry", 
        headers=self.token_auth_header)

    # delete test queue
    r = self.client.delete("/api/v1/unittest/queues/transient_queue",
        headers=self.token_auth_header)`

These examples are written in a Requests-like client (the Flask testing 
client) but should suffice to serve as an example if one wishes to use a 
different client.


API Call Curl examples
======================

- Authentication:
curl -k -c newt_cookies.txt -X POST -d 'username=user&password=pass' http://127.0.0.1:8080/api/v1/login

- Logout
curl -k -c newt_cookies.txt -X DELETE http://127.0.0.1:8080/api/v1/login

- Call to authentication required endpoint:
curl -k -b newt_cookies.txt -X GET http://127.0.0.1:8080/api/v1/site/testremote/

- Deploy code in the user account:
curl -k -b newt_cookies.txt -X POST http://127.0.0.1:8080/api/v1/site/setup/

- Read Queues summary
curl -k -b newt_cookies.txt -X GET http://127.0.0.1:8080/api/v1/gprodri/

- Create queue *mpi*
curl -k -b newt_cookies.txt -X POST http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/

- Get summary info for queue *mpi* 
curl -k -b newt_cookies.txt -X GET http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/

- Pause queue *mpi*
curl -k -b newt_cookies.txt -X PUT -d 'state=Paused' http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/

- Activate queue *mpi*
curl -k -b newt_cookies.txt -X PUT -d 'state=Active' http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/

- List tasks in queue *mpi*
curl -k -b newt_cookies.txt -X GET http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/tasks/

- Create a new task in queue *mpi*
curl -k -b newt_cookies.txt -X POST -d 'task=ls' http://127.0.0.1:8080/api/v1/gprodri/queues/mpi/tasks/

- *Launch a queue*:
curl -k -b newt_cookies.txt -X PUT -d 'mpack=1&nworkers=24&pack=True&mpack=1&batchqueue=debug' http://127.0.0.1:8080/api/v1/gprodri/queues/testq/launch/

- *Launch a queue in Hopper*:
curl -k -b newt_cookies.txt -X PUT -d 'site=hopper&mpack=1&nworkers=24&pack=True&mpack=1&batchqueue=debug' http://127.0.0.1:8080/api/v1/gprodri/queues/testq/launch/

(it supports the post parameters with the same name and meaning as qdo.launch)


Advanced server topics
======================

Running the dev server under SSL
--------------------------------

The development server invoked by run.py can be run under SSL.  Since this 
would normally be achieved in a production environment by Apache handling 
the secure transport layer, this functionality is not on by default.

The pyOpenSSL library needs to be installed to enable this.  Since this 
can cause various compilation issues, this is not included in the 
requirements.txt file.  Install pyOpenSSL and invoke:

    ./run.py --ssl

If SSL support is not enabled, there will not be a fatal error, but the 
following message will be displayed:

    ./run.py --ssl
    The pyOpenSSL lib is required for this mode - NOT currently in SSL mode.

This will just run the server in ad hoc mode, so whatever client you are 
using will need to be run in a mode that does not perform cert validation 
(curl -k|--insecure flag, etc).

It should be noted that when running the dev server under SSL, unlike a 
"real" webserver which runs http and https on different ports, this 
just runs as http OR https on the selected port.  This means that all
requests will need to be issued to the appropriate http/s address.  

This can cause some confusion in the behavior of the FORCE_SSL option 
in the config.py file.  Normally when this is set, an http request 
send to a view decorated to only allow https will be automatically 
redirected to the https version.  This won't work under when running 
the dev server in --ssl mode because there is nothing to process the 
http request.  

If FORCE_SSL is set to True, but the dev server is not run in --ssl mode,
FORCE_SSL will be set to False and the related functionality will 
be disabled.

Running the server as a WSGI app (on Apache)
--------------------------------------------
(NOT SUPPORTED IN NEW VERSION)

Deploy qdo_webserver code and make sure that dependencies are installed 
(including the QDO source code).  Dependencies can be deployed into either 
system python installation or a virtualenv.  Examples presume that code is
deployed in: /var/www/qdo_webserver

The WSGI adapter file is app.wsgi found at the root of the distro.  If code 
dependencies are installed in a virtualenv, see the following part of app.wsgi 
and modify as appropriate:

    # If using a virtualenv, set this var to a fully qualified
    # path to the virutal env bin directory

    venv_path = None
    # venv_path = '/var/www/venv/bin'

    if venv_path:
        activate_this = '{0}/activate_this.py'.format(venv_path.rstrip('/'))
        execfile(activate_this, dict(__file__=activate_this))

If running as a WSGI app in place, no further modifications need to be made 
to app.wsgi.  If for some reason you wish to have the wsgi file live somewhere 
else, see the comment in app.wsgi about modifying the sys.path.append() 
statement.

Compile and install mod_wsgi.  

    https://github.com/GrahamDumpleton/mod_wsgi/releases

Note: mod_wsgi needs to be built against the python version on the machine 
running apache.  Installing mod_wsgi using a package manager will often 
result in a version mismatch, so compiling it against the actual apache and
python on the deployment machine is recommended.

Add the appropriate directive to load the shared library into Apache httpd.conf:

    LoadModule wsgi_module libexec/apache2/mod_wsgi.so

Now hook the wsgi app into Apache with a virutal host directive.  There are a 
variety of variations of how to do this depending on how you want your webserver 
set up (do you want the app at the root of the webserver, or aliased, etc). But 
here is a virtual host directive that gives the basic configuration setttings 
that are necessary:

    <VirtualHost *:80>
        ServerName example.com

        WSGIDaemonProcess qdo_webserver user=_www group=_www threads=5
        WSGIScriptAlias / /var/www/qdo_webserver/app.wsgi
        WSGIPassAuthorization On
        
        SetEnv QDO_DB_DIR /var/www/queues

        <Directory /var/www/qdo_webserver>
            WSGIProcessGroup qdo_webserver
            WSGIApplicationGroup %{GLOBAL}
            Order deny,allow
            Allow from all
        </Directory>
    </VirtualHost>

The primary considerations here are that WSGIScriptAlias is pointed at the 
wsgi file for the app, the proper deployment Directory is set, importantly 
that WSGIPassAuthorization is On, and that the path to the QD_DB_DIR is set 
to where QDO will be saving the user queues.

WSGIDaemonProcess may need to be modified for user and group processes 
depending on how the system is configured as, well as what mode you want 
to run mod_wsgi as (embedded vs. daemon mode, etc).  The process user 
will need write access to the QDO_DB_DIR, etc.

NOTE: this exact configuration does NOT demonstrate enabling the app under 
SSL.  The webserver will need to be properly configured to serve up ssl/https 
and the proper modifications to the directive above will need to be made.

Running the server as an external FastCGI app (on Apache)
---------------------------------------------------------
(More info in docs/deployment.md)


This has also been deployed at NERSC using mod_fastcgi and the 
FastCGIExternalServer directive.  The Apache config changes were:

    LoadModule fastcgi_module modules/mod_fastcgi.so
    RewriteEngine  on
    RewriteRule    ^/qdo$ /qdo/  [R]
    FastCGIExternalServer /var/www/html/qdo -host 127.0.0.1:7676 
    
This essentially proxies a request to GET /qdo/... from Apache to a process 
running locally on port 7676.  In the root directory of qdo_webserver is 
an app.fcgi script.  When run like: ./app.fcgi it will run the external 
process that FastCGIExternalServer can bind to using this module.

To handle all of this (including setting up the proper env vars), a small 
script like this would tie it all together:

    #!/bin/sh
    export QDO_DB_DIR=/var/webapp/qdo/db_dir
    source ./venv/bin/activate
    ./qdo_webserver/app.fcgi

Authors
=======

qdo_webserver is developed by Monte Goode and later extended by Gonzalo Rodrigo
at Lawrence Berkeley National Lab. It is released under the BSD 3-clause open
source licence, see LICENSE.
    