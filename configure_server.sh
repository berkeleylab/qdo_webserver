#!/bin/bash
# Creates the environemnt and installs the libraries needed to run
# qdo_webserver. The folder where the script lays will be configured.
#
# Invocation: 
# ./configure_server.sh
#

INSTALL_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Installing python+virtualenv"

mkdir ${INSTALL_DIR}/src
mkdir ${INSTALL_DIR}/.localpython
cd ${INSTALL_DIR}/src
wget http://www.python.org/ftp/python/2.7.9/Python-2.7.9.tgz
tar -zxvf Python-2.7.9.tgz
cd Python-2.7.9


make clean
./configure --prefix=${INSTALL_DIR}/.localpython
make
make install

#install virtualenv

cd ${INSTALL_DIR}/src

wget https://pypi.python.org/packages/source/v/virtualenv/virtualenv-13.1.2.tar.gz#md5=b989598f068d64b32dead530eb25589a --no-check-certificate
tar -zxvf virtualenv-13.1.2.tar.gz
cd virtualenv-13.1.2/
${INSTALL_DIR}/.localpython/bin/python setup.py install

cd ${INSTALL_DIR}
mkdir virtualenvs
cd virtualenvs
${INSTALL_DIR}/.localpython/bin/virtualenv py2.7 --python=${INSTALL_DIR}/.localpython/bin/python2.7

echo "Activating virtualenv"
cd $INSTALL_DIR
source virtualenvs/py2.7/bin/activate

echo "Installing required libraries"
pip install -U easytools
pip install -U pip
pip install -r requirements.txt
pip install -e 'git+https://github.com/gonzalorodrigo/sremote.git#egg=sremote&subdirectory=py'
pip install -e 'git+https://bitbucket.org/berkeleylab/qdo.git#egg=qdo&subdirectory=py'

