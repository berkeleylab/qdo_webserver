#!/usr/bin/env python
# This script uses the full ssl context required by pyopenssl.
# it requires libffi5.
#
# Not required to use by now.
#
import os
import urllib3.contrib.pyopenssl
from flup.server.fcgi import WSGIServer
from qdo_webserver import app
from sremote.tools import get_module_version

BIND_ADDRESS = ('127.0.0.1', 7676)

def main():
    print 'Binding to', BIND_ADDRESS
    app.config["connector"] = 'newt'
    WSGIServer(app, bindAddress=BIND_ADDRESS).run()

if __name__ == '__main__':
    urllib3.contrib.pyopenssl.inject_into_urllib3()
    version_sremote=get_module_version("sremote")
    version_expected=0.14
    if float(get_module_version("sremote"))<version_expected:
        print "ERROR: sremote version ("+version_sremote+") not"+ \
              " good, update to >="+str(version_expected)
    else:
    	main()