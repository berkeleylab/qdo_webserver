Installing and running qdo_webserver within Apache
==================================================

Installing qdo_webserver implies two operations:

- Installing the qdo_webserver files and configure its environment.
- Installing the qdo+sremote endpoint.

QDO Websever
============

QDO webserver files should be installed in the machine were it will be run.
It does not need to be a NERSC system, only a machine that can access the NEWT
interface (i.e. connected to the Internet).

Installing files
----------------
This installation is for a FCGI execution model of the API Server run on the
port 7676. To be installed at '/project/projectdirs/cosmo/webapp/qdo/v1'

Important: run these steps from the machines that will execute the remote, 
i.e from the science portal not edison.

    cd /project/projectdirs/cosmo/webapp/qdo/v1
    git clone https://bitbucket.org/berkeleylab/qdo_webserver.git
    ./configure_server.sh
    chmod o+r -R .
  
Configuring Apache
------------------

Apache config entries to add:

    LoadModule fastcgi_module modules/mod_fastcgi.so
    FastCGIExternalServer /project/projectdirs/cosmo/webapp/qdo/v1 -host 127.0.0.1:7676
    
Updating qdo_webserver
----------------------

Updating the qdo_webserver files:
    
    cd /project/projectdirs/cosmo/webapp/qdo/v1
    git pull origin
    ./configure_server.sh
    chmod o+r -R .

Updating just the associated libraries (qdo, sremote)

    cd /project/projectdirs/cosmo/webapp/qdo/v1
    ./configure_server.sh
    chmod o+r -R .
    

QDO sremote endpoint
====================

QDO webserver can execute command within NERSC using the user's permissions. 
It uses scripts and libraries that should be deployed in a file system accessible
by the NERSC systems.
 
Installing the endpoint
-----------------------
Important: run these steps from the machines that will execute the remote
endpoint code (i.e. from edison, not from the science portal)

This instructions assume that the endpoint will be installed at 
/project/projectdirs/cosmo/webapp/qdo_sremote/v1

    cd /project/projectdirs/cosmo/webapp/qdo_sremote/v1
    /project/projectdirs/cosmo/webapp/qdo/v1/deployment/deploy_remote_endpoint.sh
    
This instructions will also update the endpoint if it is already configured.

Configuring qdo_webserver to use the endpoint
---------------------------------------------
Just edit config.py and set:

    REMOTE_SREMOTE_DIR='/project/projectdirs/cosmo/webapp/qdo_sremote/v1'

Running/stopping qdo_webserver
==============================
    
Starting:

    /project/projectdirs/cosmo/webapp/qdo_sremote/v1/start_qdo_service.sh
    
Stopping:

    /project/projectdirs/cosmo/webapp/qdo_sremote/v1/stop_qdo_service.sh

Virtual environments
====================
In order to avoid conflicts with existing versions of python or virtualenv,
the deployment/configuration scripts install python and virtualenv in user
mode.

This affects qdo_webserver and the remote endpoint. 
