"""
Configs for the qdo webserver app.
"""
import os

# App dir - don't change this value unless you
# really have a good reason to.
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Debug flag - this gets noisy so default to false
DEBUG = False

# Require appropriately decorated views to require use of ssl.
# This can be turned off for development/etc but should be 
# used in production.  See the section "Running the dev server 
# under SSL" in the README for further ramifictions.
FORCE_SSL = True

# Set this to a string to allow the flask app to log to a file - the 
# string will be the filename.
ENABLE_DEBUG_LOG = None
# ENABLE_DEBUG_LOG = 'flask_debug.log'

# Logging formatter string - season to taste.
DEBUG_LOG_FORMAT = '[%(asctime)s] %(levelname)s - %(message)s'


# Environment variables to be set through sremote before executing anything
# in the remote user's environ. Format {"var_name":"value"}
REMOTE_ENV = {} 

# List of folders to be added to the evironemnt path in the remote site.
REMOTE_PATH = []

# List of the domains that can access this service. Should be configured to
# the domains where the qdo webui is hosted.
QDO_WEBSITE_DOMAIN=["http://localhost", "https://localhost", "http://portal.nersc.gov", "https://portal.nersc.gov"]

# Force the client browser to deny access to the set cookies if ssl is not 
# used. When run in debug mode, the app sets this to false..
ENFORCE_SECURE_COOKIES=True

# Auth sremote site. this site will be the default to do auth to.
REMOTE_SITE='edison'

# File system route of the remote system where sremote entry point is installed.
# if set to *None* or *""*, it uses $HOME/.srmote
REMOTE_SREMOTE_DIR='/project/projectdirs/cosmo/webapp/qdo_sremote/test'

# File system route, relative to the remote user's home directory, where the
# tmp files are stored and also the result of the batch jobs. if set to *None*
# or *""*, it uses $HOME/.sremote
SREMOTE_HOME_TMP='.sremote'

# If SREMOTE_LOCATION_FILE is set, the try to do a discovery
# process by reading the $HOME/.sremote/SREMOTE_LOCATION_FILE and overwrites
# REMOTE_SREMOTE_DIR and REMOTE_HOME_TMP 
SREMOTE_LOCATION_FILE=".qdo"

# Enables/Disables the capacity of the setup REST call to perform install
# sremote and qdo in the remote site.
ALLOW_SETUP=False
