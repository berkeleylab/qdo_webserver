#!/bin/bash
# Stops qdo_webserver which process id is stored in qdo_service.pid
#

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
PID_FILE="qdo_service.pid"
echo "Stopping qdo REST API Service at: ${DIR}"
kill -9 $(cat "${DIR}/${PID_FILE}") 
rm "${DIR}/${PID_FILE}"
echo "Server stopped"

