#!/bin/bash
# Activates the environemnt and updates the libraries needed to run
# qdo_webserver. The folder where the script lays will be configured.
#
# Invocation: 
# ./update_server.sh
#

INSTALL_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

echo "Updating libraries"


echo "Activating virtualenv"
cd $INSTALL_DIR
source virtualenvs/py2.7/bin/activate

echo "Updating required libraries"
pip install -U easytools
pip install -U pip
pip install -r requirements.txt
pip install -Ue 'git+https://github.com/gonzalorodrigo/sremote.git#egg=sremote&subdirectory=py'
pip install -Ue 'git+https://bitbucket.org/berkeleylab/qdo.git#egg=qdo&subdirectory=py'

